package fr.gcommunity.tournoi.punishment;

import fr.gcommunity.tournoi.models.Punishment;
import org.bukkit.entity.Player;

/**
 * Created by lightdiscord on 12/04/17.
 */
public class DiePunishment extends Punishment {

    public void execute(Player player) {
        player.setHealth(0);
    }

    public DiePunishment() {
        super("die", false);
    }

}
