package fr.gcommunity.tournoi.punishment;

import fr.gcommunity.tournoi.models.Punishment;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by lightdiscord on 12/04/17.
 */
public class AutoDamagePunishment extends Punishment implements Listener {

    public List<String> sentences = new ArrayList<String>();

    public AutoDamagePunishment(Plugin plugin){
        super("auto-damage", true);
        Bukkit.getPluginManager().registerEvents(this, plugin);
        sentences.add("*Aïe* J'me suis fait un bobos");
        sentences.add("*Aïe* Mince alors, quand je tape, ça me tape :c");
    }

    public void execute(Player player){
        if (punished.contains(player)) {
            punished.remove(player);
        } else {
            punished.add(player);
        }
    }


    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) return;

        Player player = (Player) event.getDamager();
        if (!punished.contains(player)) return;

        event.setCancelled(true);

        player.damage(event.getDamage());

        player.sendMessage(new StringBuilder().append(ChatColor.GRAY).append(ChatColor.ITALIC).append(sentences.get(new Random().nextInt(sentences.size()))).toString());
    }
}
