package fr.gcommunity.tournoi.models;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lightdiscord on 12/04/17.
 */
public abstract class Punishment {

    private final String name;
    protected final List<Player> punished = new ArrayList<Player>();
    private final Boolean toggle;

    public abstract void execute(Player player);

    public Punishment(String name, Boolean toggle) {
        this.name = name;
        this.toggle = toggle;
    }

    public String getName() {
        return this.name;
    }
    public Boolean toActive(Player player) {
        return !toggle || !punished.contains(player);
    }

}
