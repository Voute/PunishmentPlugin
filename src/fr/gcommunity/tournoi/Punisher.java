package fr.gcommunity.tournoi;

import fr.gcommunity.tournoi.models.Punishment;
import fr.gcommunity.tournoi.punishment.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lightdiscord on 12/04/17.
 */
public class Punisher extends JavaPlugin {

    public Map<String, Punishment> registered = new HashMap<String, Punishment>();

    @Override
    public void onEnable() {
        System.out.println("Register punishment");
        registerPunishment(
                new DiePunishment(),
                new BlewMiningPunishment(this),
                new RegeneratePunishment(this),
                new AutoDamagePunishment(this),
                new SkyPunishment(this),
                new WhiterPunishment());

        getCommand("punisher").setExecutor(new Commando(registered));
    }

    @Override
    public void onDisable() {

    }

    public void registerPunishment(Punishment punishment) {
        registered.put(punishment.getName(), punishment);
    }

    public void registerPunishment(Punishment... punishments) {
        for (Punishment punishment : punishments) registerPunishment(punishment);
    }

}
